/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartmime',
  version: '1.0.6',
  description: 'a module to detect mime types'
}

import * as plugins from './smartmime.plugins.js';

export const binaryFileTypes = ['jpg', 'png', 'pdf'];

export const supportedFileTypes = ['json', 'html', 'svg', 'jpg', 'ts', 'js'];

export type TMimeTypes = 'image/jpeg' | 'image/svg+xml' | 'application/json' | 'text/html';

export const detectMimeType = (pathArg: string) => {
  return plugins.mimeTypes.lookup(pathArg) as TMimeTypes;
};

export const isBinary = (pathArg: string) => {
  let isBinary = false;
  for (const binaryFileType of binaryFileTypes) {
    if (isBinary) {
      continue;
    }
    isBinary = pathArg.endsWith(`.${binaryFileType}`);
  }
  return isBinary;
};

export const getEncoding = (pathArg: string) => {
  return isBinary(pathArg) ? 'binary' : 'utf8';
};

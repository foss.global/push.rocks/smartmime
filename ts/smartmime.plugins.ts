// node native scope
import * as path from 'path';

export { path };

// thirdparty scope
import mimeTypes from 'mime-types';

export { mimeTypes };

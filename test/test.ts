import { expect, tap } from '@pushrocks/tapbundle';
import * as smartmime from '../ts/index.js';

tap.test('should detect image', async () => {
  const filePath = 'file.jpg';
  const fileType = smartmime.detectMimeType(filePath);
  console.log(fileType);
  expect(smartmime.isBinary(filePath)).toBeTrue();
});

tap.test('should detect text', async () => {
  const filePath = 'file.svg';
  const fileType = smartmime.detectMimeType(filePath);
  console.log(fileType);
  expect(smartmime.isBinary(filePath)).toBeFalse();
});

tap.test('should detect json', async () => {
  const filePath = 'file.json';
  const fileType = smartmime.detectMimeType(filePath);
  console.log(fileType);
  expect(smartmime.isBinary(filePath)).toBeFalse();
});

tap.test('should detect html', async () => {
  const filePath = 'file.html';
  const fileType = smartmime.detectMimeType(filePath);
  expect(fileType).toEqual('text/html');
  console.log(fileType);
});

tap.test('should detect woff', async () => {
  const filePath = 'file.woff';
  const fileType = smartmime.detectMimeType(filePath);
  expect(fileType).toEqual('font/woff');
  console.log(fileType);
});

tap.test('should detect woff2', async () => {
  const filePath = 'file.woff2';
  const fileType = smartmime.detectMimeType(filePath);
  expect(fileType).toEqual('font/woff2');
  console.log(fileType);
});

tap.start();
